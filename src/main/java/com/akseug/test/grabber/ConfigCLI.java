package com.akseug.test.grabber;

import org.apache.commons.cli.*;

/**
 * Config calculate from CLI args.
 * <p>
 * <br><br>Created by axenty on 4/4/17.
 */
class ConfigCLI {
    private Options options = new Options();
    private CommandLineParser cmdParser = new DefaultParser();
    private boolean html = false;
    private String words;
    private String wordsFile;
    private String documentPath = "https://news.yandex.ru/computers.html";
    private boolean printStackTrace = false;

    ConfigCLI() {
        options.addOption("t", "html", false, "Parse the document as html.");
        options.addOption("w", "words", true, "Words for search (Example: \"-w apple,banana,mango\").");
        options.addOption("i", "in", true, "Path to file with words for search (Example \"-i /home/user/file\").");
        options.addOption("p", "path", true, "Path to document. Url to site or path to file. Supported schemes: http://, https://, file://. Default: https://news.yandex.ru/computers.html");
        options.addOption("v", "verbose", false, "If need print exception stacktrace and verbose msgs.");
        options.addOption("h", "help", false, "This message.");
    }

    private void printHelp() {
        HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.printHelp("java -jar grabber-<VERSION>-fat.jar", options, true);
    }

    void parseArgs(String[] args) {
        CommandLine cmd;
        try {
            cmd = cmdParser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            printHelp();
            System.exit(1);
            return;
        }
        if (cmd.hasOption("h")) {
            printHelp();
            System.exit(0);
            return;
        }
        html = cmd.hasOption("t");
        words = cmd.getOptionValue("w");
        wordsFile = cmd.getOptionValue("i");
        if (cmd.hasOption("p")) {
            documentPath = cmd.getOptionValue("p");
        }
        printStackTrace = cmd.hasOption("v");
    }

    boolean isHtml() {
        return html;
    }

    String getWords() {
        return words;
    }

    String getWordsFile() {
        return wordsFile;
    }

    String getDocumentPath() {
        return documentPath;
    }

    boolean isPrintStackTrace() {
        return printStackTrace;
    }
}
