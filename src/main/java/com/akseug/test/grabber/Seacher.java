package com.akseug.test.grabber;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Search words
 * <p>
 * Created by axenty on 4/4/17.
 */
class Seacher {

    Map<String, Integer> search(String document, String... words) {
        Pattern pattern = Pattern.compile(getStrPattern(words));
        Matcher matcher = pattern.matcher(document);

        Map<String, Integer> resultMap = new HashMap<>(words.length);
        Arrays.stream(words).forEach(word -> resultMap.put(word, 0));

        int start = 0;
        while (matcher.find(start)) {
            String group = matcher.group(1);
            int count = resultMap.get(group);
            resultMap.put(group, ++count);
            start = matcher.end();
        }
        return resultMap;
    }

    /**
     * Calculate string pattern like <i><b>\\b(words1|words2|words3|words4)\\b</b></i>
     *
     * @param words string array
     * @return regex string
     */
    private String getStrPattern(String... words) {
        int startSize = words.length * 10; //average 10 symbols on word
        StringBuilder sb = new StringBuilder(startSize);
        sb.append("\\b(");
        for (int i = 0; i < words.length; i++) {
            if (i != 0) {
                sb.append("|");
            }
            sb.append(words[i]);
        }

        sb.append(")\\b");
        return sb.toString();
    }
}
