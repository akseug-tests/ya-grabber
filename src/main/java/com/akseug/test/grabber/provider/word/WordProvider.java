package com.akseug.test.grabber.provider.word;

/**
 *
 *
 * <br><br>Created by axenty on 4/4/17.
 */
public interface WordProvider<T> {
    String[] getWords(T data) throws WordProviderException;
}
