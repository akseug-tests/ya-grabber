package com.akseug.test.grabber.provider.data;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * Provider for load document by http.
 *
 * <br>Created by axenty on 4/4/17.
 */
public class HttpDataProviderImpl implements DataProvider {

    private OkHttpClient okHttpClient = new OkHttpClient();

    @Override
    public String getDocument(String path) throws DataProviderException {
        Request request = new Request.Builder()
                .url(path)
                .get()
                .build();
        Call call = okHttpClient.newCall(request);
        try {
            Response response = call.execute();
            if (response.code() == HttpURLConnection.HTTP_OK) {
                return response.body().string();
            } else {
                throw new DataProviderException(String.format("Document isn't available. Path: %s.", path));
            }
        } catch (IOException e) {
            throw new DataProviderException(e);
        }
    }
}
