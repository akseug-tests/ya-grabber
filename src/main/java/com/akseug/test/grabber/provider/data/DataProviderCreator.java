package com.akseug.test.grabber.provider.data;

/**
 * Select DataPRovider by scheme.
 * <p>Created by axenty on 4/4/17.
 */
public class DataProviderCreator {

    public DataProvider getDataProvider(String path) throws UnknownDataProviderException {
        if (path.startsWith("http://") || path.startsWith("https://")) {
            return new HttpDataProviderImpl();
        } else if (path.startsWith("file://")) {
            return new FileDataProviderImpl();
        }
        throw new UnknownDataProviderException(String.format("Unknown provider data for path: %s.", path));
    }
}
