package com.akseug.test.grabber.provider.word;

public class WordProviderException extends Exception {
    public WordProviderException() {
    }

    public WordProviderException(String message) {
        super(message);
    }

    public WordProviderException(String message, Throwable cause) {
        super(message, cause);
    }

    public WordProviderException(Throwable cause) {
        super(cause);
    }

    public WordProviderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
