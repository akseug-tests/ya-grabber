package com.akseug.test.grabber.provider.data;

/**
 * Created by axenty on 4/4/17.
 */
public class UnknownDataProviderException extends Exception {

    public UnknownDataProviderException() {
    }

    public UnknownDataProviderException(String message) {
        super(message);
    }

    public UnknownDataProviderException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownDataProviderException(Throwable cause) {
        super(cause);
    }

    public UnknownDataProviderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
