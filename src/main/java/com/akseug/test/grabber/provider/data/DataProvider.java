package com.akseug.test.grabber.provider.data;

public interface DataProvider {
    String getDocument(String path) throws DataProviderException;
}
