package com.akseug.test.grabber.provider.data;

/**
 * Created by axenty on 4/4/17.
 */
public class DataProviderException extends Exception {
    public DataProviderException() {
    }

    public DataProviderException(String message) {
        super(message);
    }

    public DataProviderException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataProviderException(Throwable cause) {
        super(cause);
    }

    public DataProviderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
