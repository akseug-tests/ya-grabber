package com.akseug.test.grabber.provider.word;

/**
 * Provider will split of string by words.
 *
 * <br><br>Created by axenty on 4/4/17.
 */
public class StringWordProviderImpl implements WordProvider<String> {
    @Override
    public String[] getWords(String data) {
        return data.split("[^a-zA-Z_0-9а-яёА-ЯЁ]");
    }
}
