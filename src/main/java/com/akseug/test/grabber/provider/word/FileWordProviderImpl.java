package com.akseug.test.grabber.provider.word;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Provider read file and split by words.
 *
 * <br><br>Created by axenty on 4/4/17.
 */
public class FileWordProviderImpl implements WordProvider<Path> {
    @Override
    public String[] getWords(Path path) throws WordProviderException {
        try {
            String text = new String(Files.readAllBytes(path));
            return text.split("[^a-zA-Z_0-9а-яёА-ЯЁ]");
        } catch (IOException e) {
            throw new WordProviderException(e);
        }
    }
}
