package com.akseug.test.grabber.provider.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Read document from file.
 *
 * Created by axenty on 4/4/17.
 */
public class FileDataProviderImpl implements DataProvider {
    @Override
    public String getDocument(String path) throws DataProviderException {
        try {
            return new String(Files.readAllBytes(Paths.get(path.replace("file://", ""))));
        } catch (IOException e) {
            throw new DataProviderException(e);
        }
    }
}
