package com.akseug.test.grabber;

import com.akseug.test.grabber.provider.data.DataProvider;
import com.akseug.test.grabber.provider.data.DataProviderException;
import com.akseug.test.grabber.provider.word.FileWordProviderImpl;
import com.akseug.test.grabber.provider.data.DataProviderCreator;
import com.akseug.test.grabber.provider.data.UnknownDataProviderException;
import com.akseug.test.grabber.provider.word.StringWordProviderImpl;
import com.akseug.test.grabber.provider.word.WordProviderException;
import org.jsoup.Jsoup;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {

    private static final ConfigCLI CONFIG_CLI = new ConfigCLI();

    public static void main(String[] args) {
        CONFIG_CLI.parseArgs(args);

        String[] words;
        try {
            words = getWords();
            if (words.length == 0) {
                System.out.println("No words for work.");
                System.exit(21);
                return;
            } else {
                System.out.println("Words: " + Arrays.toString(words));
            }
        } catch (WordProviderException e) {
            printStackTrace(e);
            System.exit(20);
            return;
        }

        String document;
        DataProviderCreator dataProviderCreator = new DataProviderCreator();
        try {
            DataProvider dataProvider = dataProviderCreator.getDataProvider(CONFIG_CLI.getDocumentPath());
            document = dataProvider.getDocument(CONFIG_CLI.getDocumentPath());
            System.out.println(String.format("Path to document: %s", CONFIG_CLI.getDocumentPath()));
            printVerbose("Document: " + document);
        } catch (UnknownDataProviderException | DataProviderException e) {
            printStackTrace(e);
            System.exit(3);
            return;
        }

        if (CONFIG_CLI.isHtml()) {
            document = Jsoup.parse(document).text();
            printVerbose("Document without html tags: " + document);
        }

        Seacher seacher = new Seacher();
        Map<String, Integer> unsortedMap = seacher.search(document.toLowerCase(),
                prepareWords(words));

        System.out.println("\n------\nResult:");
        unsortedMap.entrySet().stream()
                .sorted((o1, o2) -> o1.getValue() > o2.getValue() ? -1 : 1)
                .forEach(System.out::println);
    }

    /**
     * 1. remove duplicate
     * 2. remove empty strings
     * 3. toLowerCase()
     *
     * @param strings string array
     * @return modified string array
     */
    private static String[] prepareWords(String[] strings) {
        Set<String> set = new HashSet<>(strings.length);
        for (String string : strings) {
            if (!string.isEmpty()) {
                set.add(string.toLowerCase());
            }
        }
        return set.stream().toArray(String[]::new);
    }


    private static void printVerbose(String str) {
        if (CONFIG_CLI.isPrintStackTrace()) {
            System.out.println(str);
        }
    }

    private static void printStackTrace(Exception e) {
        if (CONFIG_CLI.isPrintStackTrace()) {
            e.printStackTrace();
        }
    }

    private static String[] getWords() throws WordProviderException {
        String[] words = new String[0];
        if (CONFIG_CLI.getWords() != null) {
            StringWordProviderImpl stringWordProvider = new StringWordProviderImpl();
            words = stringWordProvider.getWords(CONFIG_CLI.getWords());
        } else if (CONFIG_CLI.getWordsFile() != null) {
            FileWordProviderImpl fileWordProvider = new FileWordProviderImpl();
            words = fileWordProvider.getWords(Paths.get(CONFIG_CLI.getWordsFile()));
        }
        return words;
    }

}
